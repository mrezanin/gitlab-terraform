#!/bin/bash

set -e
set -x

# download and install terraform
TERRAFORM_VERSION=0.14.8

wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip
mv terraform /usr/local/bin
rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip


# download prerequisites for go
apk add go

# compile patched terraform gitlab provider
git clone https://github.com/gitlabhq/terraform-provider-gitlab.git
cd terraform-provider-gitlab/
git config user.email "you@example.com"
git config user.name "Your Name"
git am -3 ../terraform_patches/000*

make build

mkdir -p ~/.terraform.d/plugins/redhat.com/gitlab/gitlab/3.5.1/linux_amd64
mv ~/go/bin/terraform-provider-gitlab ~/.terraform.d/plugins/redhat.com/gitlab/gitlab/3.5.1/linux_amd64

# cleanup
apk del go
cd ..
rm -rf terraform-provider-gitlab
rm -rf ~/go

# install various pip dependencies
pip install gitlab
