#!/bin/bash

set -x

ci-fairy generate-template --config config.yaml templates/ci.tmpl > .gitlab-ci.yml
ci-fairy generate-template --config config.yaml templates/gitlab-tf-image.tmpl > gitlab-tf-image.yml
ci-fairy generate-template --config config.yaml templates/make_in_container.sh.tmpl > make_in_container.sh
chmod +x make_in_container.sh
ci-fairy generate-template --config example/example.yaml gl_terraform/data/gitlab.tf.tmpl > example/gitlab.tf
